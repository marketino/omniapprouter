<?php

return [
    'url_domain' => env('URL_DOMAIN'),
    'aws_local' => [
        'url_domain' => env('AWS_LOCAL_URL_DOMAIN'),
        'port' => env('AWS_LOCAL_URL_PORT'),
    ],
];
