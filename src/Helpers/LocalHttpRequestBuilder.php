<?php

namespace Superius\OmniAppRouter\Helpers;

use Superius\OmniAppRouter\Requests\LocalHttpRequest;

/**
 * @method static LocalHttpRequest bffApp()
 * @method static LocalHttpRequest bffProxyApp()
 * @method static LocalHttpRequest fiscalizationApp()
 * @method static LocalHttpRequest orderApp()
 * @method static LocalHttpRequest resourcesApp()
 * @method static LocalHttpRequest salesApp()
 * @method static LocalHttpRequest salesReportsApp()
 * @method static LocalHttpRequest timeApp()
 * @method static LocalHttpRequest masterApp()
 */
class LocalHttpRequestBuilder extends UrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return LocalHttpRequest
     */
    public static function __callStatic(string $name, array $arguments): LocalHttpRequest
    {
        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $appUrlKey): LocalHttpRequest
    {
        $url = self::getUrlByKey($appUrlKey, true);

        return new LocalHttpRequest($url);
    }
}
