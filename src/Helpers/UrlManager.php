<?php

namespace Superius\OmniAppRouter\Helpers;

use Illuminate\Support\Str;

class UrlManager
{
    protected static function getUrlByKey(string $appUrlKey, bool $isAwsLocal = false): string
    {
        $appUrlKey = Str::replaceLast('-app', '', $appUrlKey);

        $env = app()->environment();
        $isStaging = $env === 'staging';
        $isProduction = $env === 'production';

        if ($isStaging) {
            $appUrlKey = "test-$appUrlKey";
        }

        $url = "https://$appUrlKey" . config('omniapprouter.url_domain');

        if ($isProduction || $isStaging) {
            return $isAwsLocal ? self::transferUrlToAwsLocal($url) : $url;
        }

        return "http://$appUrlKey.omniblagajna.test";
    }

    private static function transferUrlToAwsLocal(string $url): string
    {
        $httpUrl = Str::replaceFirst('https', 'http', $url);

        return Str::replaceLast(config('omniapprouter.url_domain'), config('omniapprouter.aws_local.url_domain') .
            PATH_SEPARATOR . config('omniapprouter.aws_local.port'), $httpUrl);
    }

    public static function formatUrlKey(string $urlKey): string
    {
        return Str::snake($urlKey, '-');
    }
}
