<?php

namespace Superius\OmniAppRouter\Helpers;

use Superius\OmniAppRouter\Requests\UserTokenRequest;

/**
 * @method static UserTokenRequest bffApp()
 * @method static UserTokenRequest bffProxyApp()
 * @method static UserTokenRequest fiscalizationApp()
 * @method static UserTokenRequest orderApp()
 * @method static UserTokenRequest resourcesApp()
 * @method static UserTokenRequest salesApp()
 * @method static UserTokenRequest salesReportsApp()
 * @method static UserTokenRequest timeApp()
 * @method static UserTokenRequest masterApp()
 */
class UserTokenRequestBuilder extends UrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return UserTokenRequest
     */
    public static function __callStatic(string $name, array $arguments): UserTokenRequest
    {

        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $hubUrlKey): UserTokenRequest
    {
        $url = self::getUrlByKey($hubUrlKey);

        return new UserTokenRequest($url);
    }
}
