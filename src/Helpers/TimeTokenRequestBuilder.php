<?php

namespace Superius\OmniAppRouter\Helpers;

use Superius\OmniAppRouter\Requests\TimeTokenRequest;

/**
 * @method static TimeTokenRequest bffApp()
 * @method static TimeTokenRequest bffProxyApp()
 * @method static TimeTokenRequest fiscalizationApp()
 * @method static TimeTokenRequest orderApp()
 * @method static TimeTokenRequest resourcesApp()
 * @method static TimeTokenRequest salesApp()
 * @method static TimeTokenRequest salesReportsApp()
 * @method static TimeTokenRequest timeApp()
 * @method static TimeTokenRequest masterApp()
 */
class TimeTokenRequestBuilder extends UrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return TimeTokenRequest
     */
    public static function __callStatic(string $name, array $arguments): TimeTokenRequest
    {
        return self::apiRequest(self::formatUrlKey($name));
    }

    private static function apiRequest(string $hubUrlKey): TimeTokenRequest
    {
        $url = self::getUrlByKey($hubUrlKey);

        return new TimeTokenRequest($url);
    }
}
