<?php

namespace Superius\OmniAppRouter\Helpers;

/**
 * @method static string bffApp()
 * @method static string bffProxyApp()
 * @method static string fiscalizationApp()
 * @method static string orderApp()
 * @method static string resourcesApp()
 * @method static string salesApp()
 * @method static string salesReportsApp()
 * @method static string timeApp()
 * @method static string masterApp()
 */
class UrlBuilder extends UrlManager
{
    /**
     * @param string $name
     * @param array<int,mixed> $arguments
     * @return string
     */
    public static function __callStatic(string $name, array $arguments): string
    {
        return self::getUrlByKey(self::formatUrlKey($name));
    }
}
