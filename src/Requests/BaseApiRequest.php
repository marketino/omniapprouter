<?php

namespace Superius\OmniAppRouter\Requests;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;

abstract class BaseApiRequest
{
    private $host;
    private $uri;
    private $token;

    public function __construct(string $host = null)
    {
        $this->host = $host;
    }

    /**
     * @param string $uri
     * @param bool $returnError
     * @return array<mixed>
     */
    public function getJson(string $uri, bool $returnError = false): array
    {
        $response = $this->get($uri);

        if (!$returnError && $response->clientError()) {
            return [];
        }
        return $this->jsonDecode($response->body());
    }

    public function get(string $uri): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->get($this->getUrl());
    }

    abstract public function getRequest(): PendingRequest;

    abstract public function getToken(): string;

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    private function getUrl(): string
    {
        $url = '';
        if ($this->host) {
            $url .= $this->host;
        } else {
            $url .= config('app.url');
        }

        if ($this->uri) {
            $url .= $this->uri;
        } else {
            throw new \RuntimeException('Trying to make request with empty uri');
        }
        return $url;
    }

    /**
     * @param string $input
     * @return array<mixed>
     */
    private function jsonDecode(string $input): array
    {
        return json_decode($input, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $uri
     * @param array<mixed> $data
     * @return array<mixed>
     */
    public function postJson(string $uri, array $data): array
    {
        $response = $this->post($uri, $data);
        return $this->jsonDecode($response->body());
    }

    /**
     * @param string $uri
     * @param array<mixed> $data
     * @return \Illuminate\Http\Client\Response
     */
    public function post(string $uri, array $data): Response
    {
        $this->uri = $uri;
        return $this->getRequest()->post($this->getUrl(), $data);
    }
}
