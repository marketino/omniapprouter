<?php

namespace Superius\OmniAppRouter\Requests;

use Illuminate\Support\Facades\Http;
use Superius\OmniAppMiddleware\Services\AuthTokenService;
use Illuminate\Http\Client\PendingRequest;

class TimeTokenRequest extends BaseApiRequest
{
    private ?int $mid = null;
    private ?string $tid = null;
    private bool $is_demo = false;

    public function getToken(): string
    {
        return (new AuthTokenService($this->mid, $this->tid, $this->is_demo))->toString();
    }

    public function getRequest(): PendingRequest
    {
        if (!isset($this->token) || !$this->token) {
            $this->token = $this->getToken();
        }
        return Http::withToken($this->token)->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ]);
    }

    public static function validateToken(string $token): bool
    {
        return (new AuthTokenService())->fromToken($token)->isValid();
    }

    public function addTenantContext(int $mid, string $tid, bool $is_demo = false): void
    {
        $this->mid = $mid;
        $this->tid = $tid;
        $this->is_demo = $is_demo;
    }
}
