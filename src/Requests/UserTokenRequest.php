<?php

namespace Superius\OmniAppRouter\Requests;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\PendingRequest;

class UserTokenRequest extends BaseApiRequest
{
    public function getToken(): string
    {
        //token can be via header (for API / mobile app) or cookie (for web / SPA app)
        return request()->bearerToken() ?: request()->cookie("token");
    }

    public function getRequest(): PendingRequest
    {
        if (!isset($this->token) || !$this->token) {
            $this->token = $this->getToken();
        }
        return Http::withToken($this->token)->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ]);
    }
}
