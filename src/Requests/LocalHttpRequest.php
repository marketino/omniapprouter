<?php

namespace Superius\OmniAppRouter\Requests;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Superius\OmniApp\Helpers\MarketContext;
use Illuminate\Http\Client\PendingRequest;

class LocalHttpRequest extends BaseApiRequest
{
    public function getToken(): string
    {
        return '';
    }

    public function getRequest(): PendingRequest
    {
        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Market' => MarketContext::getMarket(false),
            'Tenant' => Auth::getUser()?->tenant_id,
        ]);
    }
}
