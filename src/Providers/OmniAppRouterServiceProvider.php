<?php

namespace Superius\OmniAppRouter\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class OmniAppRouterServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->environment('local')) {
            $source = base_path('vendor/superius/omniapp-router/config/omniapprouter.php');
            $destination = base_path('config/omniapprouter.php');

            File::copy($source, $destination);
        }
    }

}
